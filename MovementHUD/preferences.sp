// =========================================================== //

int g_ChangeMsgs[MAXPLAYERS + 1];

HudMode g_KeysMode[MAXPLAYERS + 1];
HudMode g_SpeedMode[MAXPLAYERS + 1];

HudStatus g_ShowKeys[MAXPLAYERS + 1];
HudStatus g_ShowSpeed[MAXPLAYERS + 1];

HudPosition	g_KeysHudTextPosition[MAXPLAYERS + 1];
HudPosition	g_SpeedHudTextPosition[MAXPLAYERS + 1];

HudColor g_KeysHudTextColor[MAXPLAYERS + 1][KeyColor];
HudColor g_SpeedHudTextColor[MAXPLAYERS + 1][SpeedColor];

// ======================== PUBLIC =========================== //

void RegisterPreferences()
{
	gH_ChangeMsgs = 				RegClientCookie("mhud_changemsgs", "Enable/disable change messages", CookieAccess_Protected);
	
	gH_ShowSpeed = 					RegClientCookie("mhud_speed", "Enable/disable speed display", CookieAccess_Protected);
	gH_SpeedMode = 					RegClientCookie("mhud_speed_mode", "Switch speed display modes", CookieAccess_Protected);
	gH_SpeedHudTextPosition = 		RegClientCookie("mhud_speed_position", "Switch speed positions", CookieAccess_Protected);
	gH_SpeedHudTextColor_Perf = 	RegClientCookie("mhud_speed_perf_color", "Switch speed perf color", CookieAccess_Protected);
	gH_SpeedHudTextColor_Normal =   RegClientCookie("mhud_speed_normal_color", "Switch speed normal color", CookieAccess_Protected);
	
	gH_ShowKeys = 					RegClientCookie("mhud_keys", "Enable/disable key display", CookieAccess_Protected);
	gH_KeysMode = 					RegClientCookie("mhud_keys_mode", "Switch key display modes", CookieAccess_Protected);
	gH_KeysHudTextPosition = 		RegClientCookie("mhud_keys_position", "Switch key positions", CookieAccess_Protected);
	gH_KeysHudTextColor_Normal = 	RegClientCookie("mhud_keys_normal_color", "Switch keys normal color", CookieAccess_Protected);
	gH_KeysHudTextColor_Overlap = 	RegClientCookie("mhud_keys_overlap_color", "Switch keys overlap color", CookieAccess_Protected);
}

void GetCachedPreferences(int client)
{
	g_ChangeMsgs[client]							= GetCachedPreference(client, Preference_Change_Msgs);
	
	g_ShowSpeed[client] 							= GetCachedPreference(client, Preference_Speed_Toggle);
	g_SpeedMode[client] 							= GetCachedPreference(client, Preference_Speed_Mode);
	g_SpeedHudTextPosition[client]					= GetCachedPreference(client, Preference_Speed_Position);
	g_SpeedHudTextColor[client][SpeedColor_Perf] 	= GetCachedPreference(client, Preference_Speed_Color_Perf);
	g_SpeedHudTextColor[client][SpeedColor_Normal]	= GetCachedPreference(client, Preference_Speed_Color_Normal);
	
	g_ShowKeys[client]								= GetCachedPreference(client, Preference_Keys_Toggle);
	g_KeysMode[client]								= GetCachedPreference(client, Preference_Keys_Mode);
	g_KeysHudTextPosition[client] 					= GetCachedPreference(client, Preference_Keys_Position);
	g_KeysHudTextColor[client][KeyColor_Normal] 	= GetCachedPreference(client, Preference_Keys_Color_Normal);
	g_KeysHudTextColor[client][KeyColor_Overlap] 	= GetCachedPreference(client, Preference_Keys_Color_Overlap);
}

void SetPreferenceDefaults(int client)
{
	if (!IsPreferenceCached(client, Preference_Change_Msgs))
		CachePreference(client, GetCookie(Preference_Change_Msgs), 1);

	if (!IsPreferenceCached(client, Preference_Speed_Toggle))
		CachePreference(client, GetCookie(Preference_Speed_Toggle), view_as<int>(HudStatus_Disabled));
	
	if (!IsPreferenceCached(client, Preference_Speed_Mode))
		CachePreference(client, GetCookie(Preference_Speed_Mode), view_as<int>(HudMode_HudText));
	
	if (!IsPreferenceCached(client, Preference_Speed_Position))
		CachePreference(client, GetCookie(Preference_Speed_Position), view_as<int>(HudPosition_Bottom));
	
	if (!IsPreferenceCached(client, Preference_Speed_Color_Perf))
		CachePreference(client, GetCookie(Preference_Speed_Color_Perf), view_as<int>(HudColor_Green));
	
	if (!IsPreferenceCached(client, Preference_Speed_Color_Normal))
		CachePreference(client, GetCookie(Preference_Speed_Color_Normal), view_as<int>(HudColor_White));

	if (!IsPreferenceCached(client, Preference_Keys_Toggle))
		CachePreference(client, GetCookie(Preference_Keys_Toggle), view_as<int>(HudStatus_Disabled));
	
	if (!IsPreferenceCached(client, Preference_Keys_Mode))
		CachePreference(client, GetCookie(Preference_Keys_Mode), view_as<int>(HudMode_HudText));
	
	if (!IsPreferenceCached(client, Preference_Keys_Position))
		CachePreference(client, GetCookie(Preference_Keys_Position), view_as<int>(HudPosition_Middle));
	
	if (!IsPreferenceCached(client, Preference_Keys_Color_Normal))
		CachePreference(client, GetCookie(Preference_Keys_Color_Normal), view_as<int>(HudColor_White));
	
	if (!IsPreferenceCached(client, Preference_Keys_Color_Overlap))
		CachePreference(client, GetCookie(Preference_Keys_Color_Overlap), view_as<int>(HudColor_Red));
}

int GetPreferenceVal(int client, Preference preference)
{
	int value = -1;
	
	switch (preference)
	{
		case Preference_Change_Msgs: value = view_as<int>(g_ChangeMsgs[client]);
		
		case Preference_Speed_Mode: value = view_as<int>(g_SpeedMode[client]);
		case Preference_Speed_Toggle: value = view_as<int>(g_ShowSpeed[client]);
		case Preference_Speed_Position: value = view_as<int>(g_SpeedHudTextPosition[client]);
		case Preference_Speed_Color_Perf: value = g_SpeedHudTextColor[client][SpeedColor_Perf];
		case Preference_Speed_Color_Normal: value = g_SpeedHudTextColor[client][SpeedColor_Normal];
		
		case Preference_Keys_Mode: value = view_as<int>(g_KeysMode[client]);
		case Preference_Keys_Toggle: value = view_as<int>(g_ShowKeys[client]);
		case Preference_Keys_Position: value = view_as<int>(g_KeysHudTextPosition[client]);
		case Preference_Keys_Color_Normal: value = g_KeysHudTextColor[client][KeyColor_Normal];
		case Preference_Keys_Color_Overlap: value = g_KeysHudTextColor[client][KeyColor_Overlap];
	}

	return value;
}

void HandlePreference(int client, Preference preference)
{
	int newValue = -1;
	Handle cookie = GetCookie(preference);
	
	switch (preference)
	{
		case Preference_Change_Msgs:
		{
			int limit = view_as<int>(ChangeMsgs_Count) - 1;
			newValue = CyclePreference(g_ChangeMsgs[client], limit);
			
			MHud_Print(client, GetCmdReplySource(), "Preference change msgs are now \"%s\"", gC_ChangeMsgsStates[newValue]);
		}
		
		case Preference_Speed_Toggle:
		{
			int limit = view_as<int>(HudStatus_Count) - 1;
			newValue = CyclePreference(g_ShowSpeed[client], limit);

			if (g_ChangeMsgs[client]) 
				MHud_Print(client, GetCmdReplySource(), "Speed is now \"%s\"", gC_HudStatusStates[newValue]);
		}
		case Preference_Speed_Mode:
		{
			int limit = view_as<int>(HudMode_Count) - 1;
			newValue = CyclePreference(g_SpeedMode[client], limit);
			
			if (g_ChangeMsgs[client])
				MHud_Print(client, GetCmdReplySource(), "Speed mode is now \"%s\"", gC_HudModeStates[newValue]);
		}
		case Preference_Speed_Position:
		{
			int limit = view_as<int>(HudPosition_Count) - 1;
			newValue = CyclePreference(g_SpeedHudTextPosition[client], limit);

			if (g_ChangeMsgs[client])
				MHud_Print(client, GetCmdReplySource(), "Speed position is now \"%s\"", gC_HudPositionStates[newValue]);
		}
		case Preference_Speed_Color_Perf:
		{
			int limit = view_as<int>(HudColor_Count) - 1;
			newValue = CyclePreference(g_SpeedHudTextColor[client][SpeedColor_Perf], limit);

			if (g_ChangeMsgs[client])
				MHud_Print(client, GetCmdReplySource(), "Perf speed color is now \"%s\"", gC_HudColorStates[newValue]);
		}
		case Preference_Speed_Color_Normal:
		{
			int limit = view_as<int>(HudColor_Count) - 1;
			newValue = CyclePreference(g_SpeedHudTextColor[client][SpeedColor_Normal], limit);

			if (g_ChangeMsgs[client])
				MHud_Print(client, GetCmdReplySource(), "Normal speed color is now \"%s\"", gC_HudColorStates[newValue]);
		}
		case Preference_Keys_Toggle:
		{
			int limit = view_as<int>(HudStatus_Count) - 1;
			newValue = CyclePreference(g_ShowKeys[client], limit);

			if (g_ChangeMsgs[client])
				MHud_Print(client, GetCmdReplySource(), "Keys are now \"%s\"", gC_HudStatusStates[newValue]);
		}
		case Preference_Keys_Mode:
		{
			int limit = view_as<int>(HudMode_Count) - 1;
			newValue = CyclePreference(g_KeysMode[client], limit);

			if (g_ChangeMsgs[client])
				MHud_Print(client, GetCmdReplySource(), "Keys mode is now \"%s\"", gC_HudModeStates[newValue]);
		}
		case Preference_Keys_Position:
		{
			int limit = view_as<int>(HudPosition_Count) - 1;
			newValue = CyclePreference(g_KeysHudTextPosition[client], limit);

			if (g_ChangeMsgs[client])
				MHud_Print(client, GetCmdReplySource(), "Keys position is now \"%s\"", gC_HudPositionStates[newValue]);
		}
		case Preference_Keys_Color_Normal:
		{
			int limit = view_as<int>(HudColor_Count) - 1;
			newValue = CyclePreference(g_KeysHudTextColor[client][KeyColor_Normal], limit);

			if (g_ChangeMsgs[client])
				MHud_Print(client, GetCmdReplySource(), "Keys normal color is now \"%s\"", gC_HudColorStates[newValue]);
		}
		case Preference_Keys_Color_Overlap:
		{
			int limit = view_as<int>(HudColor_Count) - 1;
			newValue = CyclePreference(g_KeysHudTextColor[client][KeyColor_Overlap], limit);

			if (g_ChangeMsgs[client])
				MHud_Print(client, GetCmdReplySource(), "Keys overlap color is now \"%s\"", gC_HudColorStates[newValue]);
		}
	}
	
	if (newValue != -1)
	{
		CachePreference(client, cookie, newValue);
	}
}

// ======================== PRIVATE ========================== //

static int CyclePreference(any &num, int limit)
{
	if (num == limit)
	{
		num = 0;
	}
	else
	{
		num++;
	}

	return num;
}

// =========================================================== //