// ======================== PUBLIC =========================== //

void CreateHudHandle(Handle &hud)
{
	if (hud == null)
	{
		hud = CreateHudSynchronizer();
	}
}

void NullifyHudHandle(Handle &hud)
{
	if (hud != null)
	{
		hud = null;
	}
}

// =========================================================== //