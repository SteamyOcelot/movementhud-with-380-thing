// =========================================================== //

Handle gH_ChangeMsgs = null;

Handle gH_ShowKeys = null;
Handle gH_ShowSpeed = null;

Handle gH_KeysMode = null;
Handle gH_SpeedMode = null;

Handle gH_KeysHudTextPosition = null;
Handle gH_SpeedHudTextPosition = null;

Handle gH_KeysHudTextColor_Normal = null;
Handle gH_SpeedHudTextColor_Normal = null;

Handle gH_SpeedHudTextColor_Perf = null;
Handle gH_KeysHudTextColor_Overlap = null;

// ======================== PUBLIC =========================== //

Handle GetCookie(Preference preference)
{
	Handle cookie = null;
	
	switch (preference)
	{
		case Preference_Change_Msgs: cookie = gH_ChangeMsgs;
		
		case Preference_Speed_Mode: cookie = gH_SpeedMode;
		case Preference_Speed_Toggle: cookie = gH_ShowSpeed;
		case Preference_Speed_Position: cookie = gH_SpeedHudTextPosition;
		case Preference_Speed_Color_Perf: cookie = gH_SpeedHudTextColor_Perf;
		case Preference_Speed_Color_Normal: cookie = gH_SpeedHudTextColor_Normal;
		
		case Preference_Keys_Mode: cookie = gH_KeysMode;
		case Preference_Keys_Toggle: cookie = gH_ShowKeys;
		case Preference_Keys_Position: cookie = gH_KeysHudTextPosition;
		case Preference_Keys_Color_Normal: cookie = gH_KeysHudTextColor_Normal;
		case Preference_Keys_Color_Overlap: cookie = gH_KeysHudTextColor_Overlap;
	}
	
	return cookie;
}

bool IsPreferenceCached(int client, Preference preference)
{
	char cookie[4];
	Handle cookieHandle = GetCookie(preference);
	GetClientCookie(client, cookieHandle, cookie, sizeof(cookie));
	return !StrEqual(cookie, "");
}

void CachePreference(int client, Handle cookie, int value)
{
	char cookieValue[4];
	IntToString(value, cookieValue, sizeof(cookieValue));
	SetClientCookie(client, cookie, cookieValue);
}

any GetCachedPreference(int client, Preference preference)
{
	char cookie[4];
	Handle cookieHandle = GetCookie(preference);
	GetClientCookie(client, cookieHandle, cookie, sizeof(cookie));
	return StringToInt(cookie);
}

// =========================================================== //