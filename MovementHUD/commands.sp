// ======================= LISTENERS ========================= //

void RegisterCommands()
{
	RegConsoleCmd("sm_mhud", Command_MHud, "Opens MovementHUD preference menu");
	RegConsoleCmd("sm_mhud_changemsgs", Command_ChangeMsgs, "Enable/disable change messages");
	
	RegConsoleCmd("sm_mhud_speed", Command_ToggleSpeed, "Enable/disable speed display");
	RegConsoleCmd("sm_mhud_speed_mode", Command_SpeedMode, "Switch speed display modes");
	RegConsoleCmd("sm_mhud_speed_position", Command_SpeedPosition, "Switch speed positions");
	RegConsoleCmd("sm_mhud_speed_perf_color", Command_SpeedPerfColor, "Switch speed perf color");
	RegConsoleCmd("sm_mhud_speed_normal_color", Command_SpeedNormalColor, "Switch speed normal color");
	
	RegConsoleCmd("sm_mhud_keys", Command_ToggleKeys, "Enable/disable key display");
	RegConsoleCmd("sm_mhud_keys_mode", Command_KeysMode, "Switch key display modes");
	RegConsoleCmd("sm_mhud_keys_position", Command_KeysPosition, "Switch key positions");
	RegConsoleCmd("sm_mhud_keys_normal_color", Command_KeysNormalColor, "Switch keys normal color");
	RegConsoleCmd("sm_mhud_keys_overlap_color", Command_KeysOverlapColor, "Switch keys overlap color");
}

// ========================= PUBLIC ========================== //

public Action Command_MHud(int client, int args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}

	DisplayPreferenceMenu(client);
	return Plugin_Handled;
}

public Action Command_ChangeMsgs(int client, int args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}

	HandlePreference(client, Preference_Change_Msgs);
	return Plugin_Handled;
}

public Action Command_ToggleSpeed(int client, int args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}

	if (!gCV_Speed.BoolValue)
	{
		ReplyToCommand(client, "%s Speed display is disabled server-side!", PLUGINTAG_COLOR);
		return Plugin_Handled;
	}
	
	HandlePreference(client, Preference_Speed_Toggle);
	return Plugin_Handled;
}

public Action Command_SpeedMode(int client, int args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}
	
	HandlePreference(client, Preference_Speed_Mode);
	return Plugin_Handled;
}

public Action Command_SpeedPosition(int client, int args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}
	
	HandlePreference(client, Preference_Speed_Position);
	return Plugin_Handled;
}

public Action Command_SpeedPerfColor(int client, int args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}
	
	HandlePreference(client, Preference_Speed_Color_Perf);
	return Plugin_Handled;
}

public Action Command_SpeedNormalColor(int client, int args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}

	HandlePreference(client, Preference_Speed_Color_Normal);
	return Plugin_Handled;
}

public Action Command_ToggleKeys(int client, int args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}

	if (!gCV_Keys.BoolValue)
	{
		ReplyToCommand(client, "%s Key display is disabled server-side!", PLUGINTAG_COLOR);
		return Plugin_Handled;
	}
	
	HandlePreference(client, Preference_Keys_Toggle);
	return Plugin_Handled;
}

public Action Command_KeysMode(int client, int args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}
	
	HandlePreference(client, Preference_Keys_Mode);
	return Plugin_Handled;
}

public Action Command_KeysPosition(int client, int args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}
	
	HandlePreference(client, Preference_Keys_Position);			
	return Plugin_Handled;
}

public Action Command_KeysNormalColor(int client, int args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}
	
	HandlePreference(client, Preference_Keys_Color_Normal);
	return Plugin_Handled;
}

public Action Command_KeysOverlapColor(int client, int args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}

	HandlePreference(client, Preference_Keys_Color_Overlap);
	return Plugin_Handled;
}

// =========================================================== //