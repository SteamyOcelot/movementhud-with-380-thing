// =========================================================== //

bool	gB_Bhop[MAXPLAYERS + 1];
bool	gB_Perf[MAXPLAYERS + 1];
int 	gI_Buttons[MAXPLAYERS + 1];
float	gF_JumpSpeed[MAXPLAYERS + 1];
int		gI_GroundTicks[MAXPLAYERS + 1];
float	gF_LastJumpInput[MAXPLAYERS + 1];

// ======================= LISTENERS ========================= //

void TrackButtons(int client, int buttons)
{
	gI_Buttons[client] = buttons;
}

void TrackJumps(int client)
{
	if (OnGround(client))
	{
		IncrGroundTicks(client);
		
		if (JustJumped(client, gI_Buttons[client]))
		{
			gB_Bhop[client] = true;
			gB_Perf[client] = TicksOnGround(client, 1);
			gF_JumpSpeed[client] = GetSpeed(client);
		}
		else
		{
			gB_Bhop[client] = false;
			gB_Perf[client] = false;
		}
	}
	else
	{
		ResetGroundTicks(client);
	}
}

// ======================== PRIVATE ========================== //

static void IncrGroundTicks(int client)
{
	gI_GroundTicks[client]++;
}

static void ResetGroundTicks(int client)
{
	gI_GroundTicks[client] = 0;
}

static bool TicksOnGround(int client, int ticks)
{
	return gI_GroundTicks[client] == ticks;
}

static bool OnGround(int client)
{
	return (GetEntityFlags(client) & FL_ONGROUND == FL_ONGROUND);
}

static bool JustJumped(int client, int buttons)
{
	return (buttons & IN_JUMP == IN_JUMP) &&
			(GetEntProp(client, Prop_Data, "m_nOldButtons") & IN_JUMP != IN_JUMP);
}

// =========================================================== //