// ======================== PUBLIC =========================== //

void DisplayPreferenceMenu(int client, int atItem = 0)
{
	Menu menu = new Menu(PreferenceMenu_Handler);
	menu.SetTitle(PLUGINTAG_RAW ... " Client Preferences");
	
	PreferenceMenu_AddSpeedToggle(client, menu);
	PreferenceMenu_AddSpeedMode(client, menu);
	PreferenceMenu_AddSpeedPosition(client, menu);
	PreferenceMenu_AddSpeedColor_Perf(client, menu);
	PreferenceMenu_AddSpeedColor_Normal(client, menu);
	
	PreferenceMenu_AddKeysToggle(client, menu);
	PreferenceMenu_AddKeysMode(client, menu);
	PreferenceMenu_AddKeysPosition(client, menu);
	PreferenceMenu_AddKeysColor_Normal(client, menu);
	PreferenceMenu_AddKeysColor_Overlap(client, menu);
	
	PreferenceMenu_AddPreferenceChange(client, menu);
	
	menu.Pagination = 5;
	menu.ExitButton = true;
	menu.DisplayAt(client, atItem, MENU_TIME_FOREVER);
}

public int PreferenceMenu_Handler(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
		char itemInfo[32];
		menu.GetItem(param2, itemInfo, sizeof(itemInfo));
		Preference pref = view_as<Preference>(StringToInt(itemInfo));

		HandlePreference(param1, pref);
		DisplayPreferenceMenu(param1, param2 / menu.Pagination * menu.Pagination);
	}
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}

// ======================== PRIVATE ========================== //

static void PreferenceMenu_AddSpeedToggle(int client, Menu menu)
{
	char menuItem[32];
	Format(menuItem, sizeof(menuItem), "Speed display - %s",
			gC_HudStatusStates[GetPreferenceVal(client, Preference_Speed_Toggle)]);
	menu.AddItem(IntToStringEx(view_as<int>(Preference_Speed_Toggle)), menuItem);
}

static void PreferenceMenu_AddSpeedMode(int client, Menu menu)
{
	char menuItem[32];
	Format(menuItem, sizeof(menuItem), "Speed mode - %s",
			gC_HudModeStates[GetPreferenceVal(client, Preference_Speed_Mode)]);
	menu.AddItem(IntToStringEx(view_as<int>(Preference_Speed_Mode)), menuItem);
}

static void PreferenceMenu_AddSpeedPosition(int client, Menu menu)
{
	char menuItem[32];
	Format(menuItem, sizeof(menuItem), "Speed position - %s",
			gC_HudPositionStates[GetPreferenceVal(client, Preference_Speed_Position)]);
	menu.AddItem(IntToStringEx(view_as<int>(Preference_Speed_Position)), menuItem);
}

static void PreferenceMenu_AddSpeedColor_Perf(int client, Menu menu)
{
	char menuItem[32];
	Format(menuItem, sizeof(menuItem), "Speed perf color - %s",
			gC_HudColorStates[GetPreferenceVal(client, Preference_Speed_Color_Perf)]);
	menu.AddItem(IntToStringEx(view_as<int>(Preference_Speed_Color_Perf)), menuItem);
}

static void PreferenceMenu_AddSpeedColor_Normal(int client, Menu menu)
{
	char menuItem[32];
	Format(menuItem, sizeof(menuItem), "Speed normal color - %s",
			gC_HudColorStates[GetPreferenceVal(client, Preference_Speed_Color_Normal)]);
	menu.AddItem(IntToStringEx(view_as<int>(Preference_Speed_Color_Normal)), menuItem);
}

static void PreferenceMenu_AddKeysToggle(int client, Menu menu)
{
	char menuItem[32];
	Format(menuItem, sizeof(menuItem), "Keys display - %s",
			gC_HudStatusStates[GetPreferenceVal(client, Preference_Keys_Toggle)]);
	menu.AddItem(IntToStringEx(view_as<int>(Preference_Keys_Toggle)), menuItem);
}

static void PreferenceMenu_AddKeysMode(int client, Menu menu)
{
	char menuItem[32];
	Format(menuItem, sizeof(menuItem), "Keys mode - %s",
			gC_HudModeStates[GetPreferenceVal(client, Preference_Keys_Mode)]);
	menu.AddItem(IntToStringEx(view_as<int>(Preference_Keys_Mode)), menuItem);
}

static void PreferenceMenu_AddKeysPosition(int client, Menu menu)
{
	char menuItem[32];
	Format(menuItem, sizeof(menuItem), "Keys position - %s",
			gC_HudPositionStates[GetPreferenceVal(client, Preference_Keys_Position)]);
	menu.AddItem(IntToStringEx(view_as<int>(Preference_Keys_Position)), menuItem);
}

static void PreferenceMenu_AddKeysColor_Normal(int client, Menu menu)
{
	char menuItem[32];
	Format(menuItem, sizeof(menuItem), "Keys normal color - %s",
			gC_HudColorStates[GetPreferenceVal(client, Preference_Keys_Color_Normal)]);
	menu.AddItem(IntToStringEx(view_as<int>(Preference_Keys_Color_Normal)), menuItem);
}

static void PreferenceMenu_AddKeysColor_Overlap(int client, Menu menu)
{
	char menuItem[32];
	Format(menuItem, sizeof(menuItem), "Keys overlap color - %s",
			gC_HudColorStates[GetPreferenceVal(client, Preference_Keys_Color_Overlap)]);
	menu.AddItem(IntToStringEx(view_as<int>(Preference_Keys_Color_Overlap)), menuItem);
}

static void PreferenceMenu_AddPreferenceChange(int client, Menu menu)
{
	char menuItem[32];
	Format(menuItem, sizeof(menuItem), "Preference changes - %s", 
			gC_ChangeMsgsStates[GetPreferenceVal(client, Preference_Change_Msgs)]);
	menu.AddItem(IntToStringEx(view_as<int>(Preference_Change_Msgs)), menuItem);
}

// =========================================================== //