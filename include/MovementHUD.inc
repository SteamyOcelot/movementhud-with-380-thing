// ================== DOUBLE INCLUDE ========================= //

#if defined _MovementHUD_included_
#endinput
#endif
#define _MovementHUD_included_

// ====================== DEFINITIONS ======================== //

#define PLUGIN_NAME "MovementHUD"
#define PLUGINTAG_RAW "[MovementHUD]"
#define PLUGINTAG_COLOR "[\x0CMovement\x07HUD\x01]"

#define SPEC_MODE_INEYE 4
#define SPEC_MODE_CHASE 5
#define MAX_PRINT_MESSAGE_LENGTH 256

// ======================= ENUMS ============================= //

enum SpeedColor
{
	SpeedColor_Perf,
	SpeedColor_Normal,
};

enum KeyColor
{
	KeyColor_Normal,
	KeyColor_Overlap
};

enum ChangeMsgs
{
	ChangeMsgs_Disabled,
	ChangeMsgs_Enabled,
	ChangeMsgs_Count
};

enum HudStatus
{
	HudStatus_Disabled,
	HudStatus_Enabled,
	HudStatus_Count
};

enum HudMode
{
	HudMode_HudText = 0,
	HudMode_HudPanel,
	HudMode_Count
};

enum HudColor
{
	HudColor_Red = 0,
	HudColor_Green,
	HudColor_Lime,
	HudColor_Blue,
	HudColor_Cyan,
	HudColor_Yellow,
	HudColor_Orange,
	HudColor_Purple,
	HudColor_White,
	HudColor_Black,
	HudColor_Gray,
	HudColor_Count
};

enum HudPosition
{
	HudPosition_Bottom = 0,
	HudPosition_BottomLeft,
	HudPosition_BottomRight,
	HudPosition_Middle,
	HudPosition_MiddleLeft,
	HudPosition_MiddleRight,
	HudPosition_Top,
	HudPosition_TopRight,
	HudPosition_Count
};

enum Preference
{
	Preference_Change_Msgs = 0,
	Preference_Speed_Toggle,
	Preference_Speed_Mode,
	Preference_Speed_Position,
	Preference_Speed_Color_Perf,
	Preference_Speed_Color_Normal,
	Preference_Keys_Toggle,
	Preference_Keys_Mode,
	Preference_Keys_Position,
	Preference_Keys_Color_Normal,
	Preference_Keys_Color_Overlap,
	Preference_Count
};

// ======================== PHRASES ========================== //

char gC_ChangeMsgsStates[ChangeMsgs_Count][] =
{
	"Hidden",
	"Visible"
};

char gC_HudStatusStates[HudStatus_Count][] =
{
	"Hidden",
	"Visible"
};

char gC_HudModeStates[HudMode_Count][] =
{
	"Hud Text",
	"Hud Panel"
};

char gC_HudColorStates[HudColor_Count][] =
{
	"Red",
	"Green",
	"Lime",
	"Blue",
	"Cyan",
	"Yellow",
	"Orange",
	"Purple",
	"White",
	"Black",
	"Gray"
};

char gC_HudPositionStates[HudPosition_Count][] =
{
	"Bottom",
	"Bottom left",
	"Bottom right",
	"Middle",
	"Middle left",
	"Middle right",
	"Top",
	"Top right"
};

// ======================= VALUES ============================ //

int gI_HudColorValues[HudColor_Count][] =
{
	{ 255, 	0, 		0, 		255 },
	{ 0, 	255, 	0, 		255 },
	{ 75,   100,    0,      255 },
	{ 0, 	0, 		255, 	255 },
	{ 0,    255,    255,    255 },
	{ 255, 	215, 	0, 		255 },
	{ 255, 	165, 	0, 		255 },
	{ 128,  0,      128,    255 },
	{ 255, 	255, 	255,	255 },
	{ 0, 	0, 		0, 		255 },
	{ 128,  128,    128,    255 }
};

float gF_HudPositionValues[HudPosition_Count][] =
{
	{ -1.0, 0.85 },
	{ 0.10, 0.90 },
	{ 0.66, 0.90 },
	{ -1.0, 0.70 },
	{ 0.10, 0.70 },
	{ 0.66, 0.70 },
	{ -1.0, 0.10 },
	{ 0.66, 0.10 }
};

// ========================= STOCKS ========================== //

stock bool IsValidClient(int client, bool botsValid = false)
{
	if (botsValid)
	{
		return client >= 1 && client <= MaxClients && IsClientInGame(client);
	}
	else
	{
		return client >= 1 && client <= MaxClients && IsClientInGame(client) && !IsFakeClient(client);
	}
}

stock int GetSpecTarget(int client)
{
	return GetEntPropEnt(client, Prop_Send, "m_hObserverTarget");
}

stock bool IsSpectating(int client)
{
	return (GetClientTeam(client) == CS_TEAM_SPECTATOR);
}

stock bool IsFollowingSomeone(int client)
{
	int mode = GetEntProp(client, Prop_Send, "m_iObserverMode");
	return (mode == SPEC_MODE_INEYE || mode == SPEC_MODE_CHASE);
}

stock float GetSpeed(int client)
{
	float speed[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", speed);
	return SquareRoot(Pow(speed[0], 2.0) + Pow(speed[1], 2.0));
}

stock char[] IntToStringEx(int num)
{
	char string[12];
	IntToString(num, string, sizeof(string));
	return string;
}

stock void MHud_Print(int client, ReplySource source, char[] format, any ...)
{
	char formattedMsg[MAX_PRINT_MESSAGE_LENGTH];
	VFormat(formattedMsg, sizeof(formattedMsg), format, 4);
	
	switch (source)
	{
		case SM_REPLY_TO_CHAT:
		{
			PrintToChat(client, "%s %s", PLUGINTAG_COLOR, formattedMsg);
		}
		case SM_REPLY_TO_CONSOLE:
		{
			PrintToConsole(client, "%s %s", PLUGINTAG_RAW, formattedMsg);
		}
	}
}

// ======================= PLUGIN INFO ======================= //

public SharedPlugin __pl_MovementHUD =
{
	name = "MovementHUD",
	file = "MovementHUD.smx",
	#if defined REQUIRE_PLUGIN
	required = 1,
	#else
	required = 0,
	#endif
};

// =========================================================== //