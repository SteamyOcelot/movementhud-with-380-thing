// =========================================================== //

#include <cstrike>
#include <clientprefs>
#include <MovementHUD>

// ====================== VARIABLES ========================== //

bool   gB_LateLoaded = false;
Handle gH_KeysHudText[MAXPLAYERS + 1];
Handle gH_SpeedHudText[MAXPLAYERS + 1];

// ======================= INCLUDES ========================== //

#include "MovementHUD/misc.sp"
#include "MovementHUD/cookies.sp"
#include "MovementHUD/convars.sp"
#include "MovementHUD/commands.sp"
#include "MovementHUD/tracking.sp"
#include "MovementHUD/preferences.sp"
#include "MovementHUD/menus/preferencemenu.sp"

// ====================== PLUGIN INFO ======================== //

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = "Sikari",
	description = "",
	version = "1.0.0",
	url = "https://bitbucket.org/Sikarii/MovementHUD"
};

// ======================= MAIN CODE ========================= //

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegisterConvars();
	RegisterCommands();
	
	RegPluginLibrary(PLUGIN_NAME);
	AutoExecConfig(true, PLUGIN_NAME);
	
	gB_LateLoaded = late;
}

public void OnPluginStart()
{
	RegisterPreferences();

	if (gB_LateLoaded)
	{
		for (int i = 1; i <= MaxClients; i++)
		{
			if (IsClientConnected(i))
			{
				OnClientConnected(i);
			}
			if (AreClientCookiesCached(i))
			{
				OnClientCookiesCached(i);
			}
		}
	}
			
	gB_LateLoaded = false;
}

public void OnClientConnected(int client)
{	
	if (IsFakeClient(client))
	{
		return;
	}
	
	SetPreferenceDefaults(client);
}

public void OnClientCookiesCached(int client)
{
	if (IsFakeClient(client))
	{
		return;
	}

	GetCachedPreferences(client);
}

public void OnClientDisconnect(int client)
{
	gB_Bhop[client] = false;
	gB_Perf[client] = false;
	gI_Buttons[client] = 0;
	gF_JumpSpeed[client] = 0.0;
	gI_GroundTicks[client] = 0;
	gF_LastJumpInput[client] = 0.0;

	NullifyHudHandle(gH_KeysHudText[client]);
	NullifyHudHandle(gH_SpeedHudText[client]);
}

public void OnPlayerRunCmd(int client, int &buttons, int &impulse, const float vel[3], const float angles[3], int &weapon, int &subtype, int &cmdnum, int &	tickcount, int &seed, const int mouse[2])
{
	if (!gCV_Speed.BoolValue && !gCV_Keys.BoolValue)
	{
		return;
	}

	if (!IsValidClient(client, true))
	{
		return;
	}

	int target = client;

	if (IsSpectating(client) && IsFollowingSomeone(client))
	{
		target = GetSpecTarget(client);
	}

	else
	{
		TrackButtons(client, buttons);
		TrackJumps(client);
	}
	
	DrawKeysPanel(client, target);
	DrawSpeedPanel(client, target);
}

// ========================= PRIVATE ========================= //

static void DrawSpeedPanel(int player, int target)
{
	if (!gCV_Speed.BoolValue || !g_ShowSpeed[player])
	{
		return;
	}

	if (!IsValidClient(player) || !IsValidClient(target, true))
	{
		return;
	}

	bool perf = gB_Perf[target];
	bool bhop = gB_Bhop[target];

	float oldSpeed = gF_JumpSpeed[target];
	float currentSpeed = GetSpeed(target);
	
	if(oldSpeed > 380.00)
	{
		oldSpeed = 380.00;
	}
	
	switch (g_SpeedMode[player])
	{
		case HudMode_HudText:
		{
			CreateHudHandle(gH_SpeedHudText[player]);
			
			float position[2];
			position = gF_HudPositionValues[GetPreferenceVal(player, Preference_Speed_Position)];
			
			int normColor[4];
			normColor = gI_HudColorValues[GetPreferenceVal(player, Preference_Speed_Color_Normal)];

			int perfectColor[4];
			perfectColor = gI_HudColorValues[GetPreferenceVal(player, Preference_Speed_Color_Perf)];

			SetHudTextParamsEx(position[0], position[1], 1.0, perf ? perfectColor : normColor, _, 0, 1.0, 0.0, 0.0);

			if (!bhop)
			{
				ShowSyncHudText(player, gH_SpeedHudText[player], "\n%.2f", currentSpeed);
			}
			else
			{
				ShowSyncHudText(player, gH_SpeedHudText[player], "\n%.2f\n(%.2f)", currentSpeed, oldSpeed);
			}
		}
		case HudMode_HudPanel:
		{
			// Kill any previous handles
			// TODO: Implement this
		}
	}
}

static void DrawKeysPanel(int player, int target)
{
	if (!gCV_Keys.BoolValue || !g_ShowKeys[player])
	{
		return;
	}

	if (!IsValidClient(player) || !IsValidClient(target, true))
	{
		return;
	}

	int buttons = gI_Buttons[target];
	float lastJumpInput = gF_LastJumpInput[target];
		
	if (buttons & IN_JUMP)
	{
		gF_LastJumpInput[player] = GetEngineTime();
	}

	bool holdingS = (buttons & IN_BACK == IN_BACK);
	bool holdingC = (buttons & IN_DUCK == IN_DUCK);
	bool holdingW = (buttons & IN_FORWARD == IN_FORWARD);
	bool holdingA = (buttons & IN_MOVELEFT == IN_MOVELEFT);
	bool holdingD = (buttons & IN_MOVERIGHT == IN_MOVERIGHT);
	bool holdingJ = ((GetEngineTime() - lastJumpInput) < 0.05);
	bool overlapped = (holdingA && holdingD) || (holdingW && holdingS);
		
	char Key_W[2] = "W";
	char Key_A[2] = "A";
	char Key_S[2] = "S";
	char Key_D[2] = "D";
	char Key_C[2] = "C";
	char Key_J[2] = "J";

	if (!holdingW) Key_W = "—";
	if (!holdingA) Key_A = "—";
	if (!holdingS) Key_S = "—";
	if (!holdingD) Key_D = "—";
	if (!holdingJ) Key_J = "—";
	if (!holdingC) Key_C = "—";
	
	switch (g_KeysMode[player])
	{
		case HudMode_HudText:
		{
			CreateHudHandle(gH_KeysHudText[player]);
			
			float position[2];
			position = gF_HudPositionValues[GetPreferenceVal(player, Preference_Keys_Position)];

			int normColor[4];
			normColor = gI_HudColorValues[GetPreferenceVal(player, Preference_Keys_Color_Normal)];
			
			int overlapColor[4];
			overlapColor = gI_HudColorValues[GetPreferenceVal(player, Preference_Keys_Color_Overlap)];

			SetHudTextParamsEx(position[0], position[1], 1.0, overlapped ? overlapColor : normColor, _, 0, 1.0, 0.0, 0.0);
			ShowSyncHudText(player, gH_KeysHudText[player], "%s  %s  %s\n%s  %s  %s", Key_J, Key_W, Key_C, Key_A, Key_S, Key_D);
		}
		case HudMode_HudPanel:
		{
			// Kill any previous handles
			// TODO: Implement this
		}
	}
}

// =========================================================== //